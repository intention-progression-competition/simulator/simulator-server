// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.time.Instant;

public class ProcessCommand {

    private JSONObject jsonObject;

    public DomainManager domain;

    public ProcessCommand(DomainManager domain) {
        this.domain = domain;
    }

    /**
     * Processes JSON commands from the BDI client. These effectively represent initiation, action and termination.
     * @param clientInstructions which holds the clients original command as a (JSON) string.
     * @return CommandResponse which represents all the information to be sent back to the client.
     */
    public CommandResponse processCommand(String clientInstructions) {

        domain.getLogger().logEvent("Command Received: " + clientInstructions);

        CommandResponse cmdResponse = new CommandResponse(this.domain);

        try {
            jsonObject = new JSONObject(clientInstructions);
            String command = jsonObject.getString("command");

            if (jsonObject.has("ticktime")) {
                Long timeTaken = jsonObject.getLong("ticktime");
                domain.addTickTime(timeTaken);
            }

            switch (command) {

                case "initialise":
                    cmdResponse.setMessage("Initialisation started");
                    cmdResponse.setCommandState(CommandResponse.CommandState.VALID_COMMAND);

                    if (jsonObject.has("seed")) {
                        domain.setRandomSeed(jsonObject.getLong("seed"));
                    }

                    if (jsonObject.has("gptfile")) {

                        String fileName = jsonObject.getString("gptfile");

                        if (!this.gptFileExists(fileName)) {
                            cmdResponse.setMessage("The GPT File " + fileName + " was not found.");
                            cmdResponse.setCommandState(CommandResponse.CommandState.INVALID_GPT_FILE);
                            domain.getLogger().logEvent("Incorrect GPT file (" + fileName + ") requested");
                        } else {
                            domain.setGptFile(fileName);
                        }
                    }

                    if (jsonObject.has("timeout")) {
                        domain.setTimeoutMinutes(jsonObject.getInt("timeout"));
                    }

                    cmdResponse.setGptFile(domain.getGptFile());
                    cmdResponse.setGptContents(domain.getGptContents());
                    cmdResponse.setIncludeGPT(true);

                    domain.initiateDomain();
                    domain.updateCompetitionTime();
                    cmdResponse.setEnvironment(domain.getEnvironment());

                    domain.getLogger().logEvent("Client requested initialisation");

                    break;

                case "action":
                    try {
                        String actionName = jsonObject.getString("action");

                        if (domain.runAction(actionName)) {
                            cmdResponse.setMessage("Action: " + actionName + " completed successfully");
                            cmdResponse.setCommandState(CommandResponse.CommandState.VALID_COMMAND);
                        } else {
                            cmdResponse.setMessage("Action: " + actionName + " failed");
                            cmdResponse.setCommandState(CommandResponse.CommandState.ACTION_FAILED);
                        }

                        domain.getLogger().logEvent("Client requested action: " + actionName);

                    } catch (Exception e) {
                        domain.getLogger().logError("Missing action details: " + e.getMessage());
                    }

                    break;

                case "quit":
                    cmdResponse.setMessage("Server terminating connection");
                    cmdResponse.setCommandState(CommandResponse.CommandState.TERMINATE);

                    domain.getLogger().logEvent("Client requested termination");
                    domain.getLogger().writeFooterInfo();

                    cmdResponse.activateLogReturn(domain);

                    break;

                default:
                    cmdResponse.setMessage("Command not recognised");
                    cmdResponse.setCommandState(CommandResponse.CommandState.COMMAND_NOT_RECOGNISED);

                    domain.getLogger().logEvent("Client requested unrecognised command: " + command);
                    break;

            }
        } catch (JSONException exception) {

            String errorMessage;

            switch (exception.getMessage()) {

                case "JSONObject[\"clientid\"] not found.":
                    errorMessage = "Client ID is missing";
                    break;

                case "JSONObject[\"command\"] not found.":
                    errorMessage = "Command is missing";
                    break;

                default:
                    errorMessage = "Command sent is not valid JSON. " + exception.getMessage();
                    break;
            }

            cmdResponse.setMessage(errorMessage);
            cmdResponse.setCommandState(CommandResponse.CommandState.INVALID_COMMAND);

            domain.getLogger().logEvent("Invalid JSON received: " + clientInstructions);
        } finally {

            domain.getLogger().logEvent("Command response: " + cmdResponse.getMessage());
            domain.getLogger().logEvent("Full Response: " + cmdResponse.getAsJSONStringResponse());
        }
        
        return cmdResponse;
    }

    /**
     * Fetches the clientId from the received instructions (undertaken outside of the ProcessCommand
     * object for early access.
     * @param clientInstructions
     * @return string representing the clientId of the attempt
     */
    public static String retrieveClientId(String clientInstructions) {

        String clientId;

        try {
            JSONObject jsonObject = new JSONObject(clientInstructions);
            clientId = jsonObject.getString("clientid");
        } catch (JSONException exception) {
            clientId = "";
            //TODO - handle properly
        }

        return clientId;
    }

    /**
     * Fetches the instanceId from the received instructions (undertaken outside of the ProcessCommand
     * object for early access.
     * @param clientInstructions
     * @return string representing the instanceId of the attempt
     */
    public static String retrieveInstanceId(String clientInstructions) {

        String instanceId;

        try {
            JSONObject jsonObject = new JSONObject(clientInstructions);
            instanceId = jsonObject.getString("instanceid");
        } catch (JSONException exception) {
            instanceId = "";
            //TODO - handle properly
        }

        return instanceId;
    }


    /**
     * Checks if File exists in File System
     * @param fileName
     * @return boolean representing if file exists
     */
    private boolean gptFileExists(String fileName) {
        return new File(fileName).isFile();
    }
}
