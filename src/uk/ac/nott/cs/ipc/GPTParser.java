// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles all the parsing of GPT XML files (as produced by
 */
public class GPTParser {

    private final static Logger LOGGER = Logger.getLogger(ProcessCommand.class.getName());

    HashMap<String, Action> actions;
    HashMap<String, Literal> literalStore;
    HashMap<String, Goal> goals;

    private Environment environment;

    public GPTParser() {
        actions = new HashMap<>();
        literalStore = new HashMap<>();
        goals = new HashMap<>();
        environment = new Environment();
    }

    public void parseGPTFile(String gptXMLFile)
    {
        try {

            System.out.println("Parsing " + gptXMLFile);

            /* TODO: Add error handling in case file not specified */
            File inputFile = new File(gptXMLFile);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            GPTHandler gptHandler = new GPTHandler();
            saxParser.parse(inputFile, gptHandler);

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }
    }

    public Environment getEnvironment()
    {
        environment.setActions(actions);
        environment.setLiterals(literalStore);
        environment.setGoals(goals);

        return environment;
    }

    class GPTHandler extends DefaultHandler {

        Goal topLevelGoal;
        int goalDepth = 0, searchDepth = Integer.MAX_VALUE, execPathLength;
        boolean execPath = true;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {

            if (qName.equalsIgnoreCase("Goal")) {

                if (goalDepth == 0) {
                    execPathLength = 0;
                    searchDepth = Integer.MAX_VALUE;
                    topLevelGoal = new Goal(attributes.getValue("name"));
                    topLevelGoal.getSuccessConditions().addAll(
                            parseConditions(attributes.getValue("goal-condition")));
                }
                goalDepth++;
                goals.put(topLevelGoal.getName(),topLevelGoal);

            } else if (qName.equalsIgnoreCase("Plan")) {
                if (execPath){
                    searchDepth = goalDepth;
                }
            } else if (qName.equalsIgnoreCase("Action")) {

                if(execPath){
                    execPathLength++;
                }

                Action workingAction = new Action(attributes.getValue("name"));
                workingAction.getPreConditions().addAll(
                        parseConditions(attributes.getValue("precondition")));
                workingAction.getPostConditions().addAll(
                        parseConditions(attributes.getValue("postcondition")));

                actions.put(workingAction.getName(), workingAction);
            } else if (qName.equalsIgnoreCase("Literal")){
                boolean state, randomInit;
                if (attributes.getValue("initVal").equals("random")){
                    state = false;
                    randomInit = true;
                } else {
                    state = Boolean.parseBoolean(attributes.getValue("initVal"));
                    randomInit = false;
                }
                Literal environmentLiteral = new Literal(
                        attributes.getValue("name"),
                        state,
                        Boolean.parseBoolean(
                                attributes.getValue("stochastic")),
                        randomInit
                );
                literalStore.put(environmentLiteral.getId(), environmentLiteral);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if (qName.equalsIgnoreCase("Goal")) {
                goalDepth--;

                if (goalDepth == 0){
                    topLevelGoal.setExecPathLength(execPathLength);
                }

                if (goalDepth < searchDepth){
                    execPath = true;
                }
            } else if (qName.equalsIgnoreCase("Plan")){
                execPath = false;
                searchDepth = Math.min(goalDepth, searchDepth);
            }
        }

        @Override
        public void characters(char ch[], int start, int length) {

        }

        private Literal parseCondition(String condition)
        {
            String[] parts = condition.substring(1, condition.length()-1).split(",");
            return new Literal(parts[0], Boolean.parseBoolean(parts[1]));
        }

        private ArrayList<Literal> parseConditions(String conditions)
        {
            String[] conds = conditions.substring(0, conditions.length()-1).split(", ");
            ArrayList<Literal> literals = new ArrayList<>();

            for (String condition: conds) {
                literals.add(parseCondition(condition));
            }
            return literals;
        }
    }
}
