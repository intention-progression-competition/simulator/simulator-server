// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class represents the Environment and contains all related data.
 */
public class Environment {

    private HashMap<String, Action> actions;
    private HashMap<String, Literal> literals;
    private ArrayList<String> stochasticLiterals;

    private HashMap<String, Goal> goals;
    private ArrayList<String> pendingGoals;
    private ArrayList<String> activeGoals;
    private ArrayList<String> completedGoals;

    public Environment() {
        goals = new HashMap<>();
        pendingGoals = new ArrayList<>();
        activeGoals = new ArrayList<>();
        completedGoals = new ArrayList<>();
        stochasticLiterals = new ArrayList<>();
    }

    public HashMap<String, Action> getActions() {
        return actions;
    }

    public void setActions(HashMap<String, Action> actions) {
        this.actions = actions;
    }

    public HashMap<String, Literal> getLiterals() {
        return literals;
    }

    public void setLiterals(HashMap<String, Literal> literals) {
        this.literals = literals;
    }

    public ArrayList<String> getStochasticLiterals() {
        return stochasticLiterals;
    }

    /**
     *
     * @return  The environment represented as a JSON Object mainly in order to be
     *          communicated back to the BDI client.
     *
     **/
    public JSONObject getEnvironmentAsJSON() {

        JSONObject jsonEnvironment = new JSONObject();
        JSONObject jsonLiterals = new JSONObject();
        JSONObject jsonCurrentGoals = new JSONObject();

        for (HashMap.Entry<String, Literal> entry : this.getLiterals().entrySet()) {
            jsonLiterals.put(entry.getKey(), entry.getValue().getState());
        }

        for (String goalName: this.getActiveGoals()) {
            jsonCurrentGoals.put(goalName, Boolean.toString(false));
        }
        for (String goalName: this.getCompletedGoals()) {
            jsonCurrentGoals.put(goalName, Boolean.toString(true));
        }

        jsonEnvironment.append("literals", jsonLiterals);
        jsonEnvironment.append("goals", jsonCurrentGoals);

        return jsonEnvironment;
    }

    public HashMap<String, Goal> getGoals() {
        return goals;
    }

    public void setGoals(HashMap<String, Goal> goals) {
        this.goals = goals;
    }

    public ArrayList<String> getPendingGoals() {
        return pendingGoals;
    }

    public ArrayList<String> getActiveGoals() {
        return activeGoals;
    }

    public ArrayList<String> getCompletedGoals() {
        return completedGoals;
    }


}
