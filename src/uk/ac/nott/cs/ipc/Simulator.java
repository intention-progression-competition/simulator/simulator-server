// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * This is the main simulator class used to start the simulator and launch the socket for listening.
 */
public class Simulator {

    static int mainPortNumber = 30000;

    public enum SimulatorMode {
        NORMAL, COMPETITION
    }

    static SimulatorMode currentSimulatorMode;

    public static void main(String args[]){

        Socket socket;
        ServerSocket serverSocket = null;

        currentSimulatorMode = SimulatorMode.NORMAL;

        int i = 0;
        String arg;

        while(i < args.length && args[i].startsWith("-")){
            arg = args[i++];
            if(arg.length() != 2) {
                System.out.println(arg + " is not a valid flag");
                System.exit(1);
            }

            char flag = arg.charAt(1);
            switch (flag) {
                case 'P': // simulator port
                    mainPortNumber = Integer.parseInt(args[i++]);
                    break;

                case 'm': // competition mode
                    if (String.valueOf(args[i++]).equals("competition")) {
                        currentSimulatorMode = SimulatorMode.COMPETITION;
                    }
                    break;
            }
        }

        System.out.println("Server Listening on port " + Simulator.mainPortNumber);

        try{
            serverSocket = new ServerSocket(Simulator.mainPortNumber);
        }
        catch(IOException e){
            e.printStackTrace();
            System.out.println("Server error");
            System.exit(1);
        }

        ExecutorService poolExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        while (true) {
            try {
                socket = serverSocket.accept();
                System.out.println("Connection established");

                poolExecutor.execute(new ServerThread(socket, currentSimulatorMode));
            }

            catch(Exception e){
                e.printStackTrace();
                System.out.println("Connection Error");
            }
        }
    }
}
