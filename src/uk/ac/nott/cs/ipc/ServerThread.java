// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerThread implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(ProcessCommand.class.getName());

    String command;
    BufferedReader inputStream;
    PrintWriter outputStream;
    Socket socket;
    Simulator.SimulatorMode currentSimulatorMode;

    HashMap<String, DomainManager> domainManagers;

    public ServerThread(Socket socket, Simulator.SimulatorMode simulatorMode){
        System.out.println("Server Thread Started in " + simulatorMode.toString() + " mode");

        this.socket = socket;
        this.currentSimulatorMode = simulatorMode;
        this.domainManagers = new HashMap<String, DomainManager>();
    }

    /**
     * Main thread to manage socket. Uses the DomainManager to handle Environment interactions
     */
    public void run() {
        try {
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outputStream = new PrintWriter(socket.getOutputStream());
        } catch(IOException e) {
            LOGGER.log(Level.SEVERE, "IO error in server thread");
        }

        try {

            boolean listeningState = true;

            while (listeningState) {
                command = inputStream.readLine();

                DomainManager domainManager = this.getDomainManager(
                        ProcessCommand.retrieveClientId(command),
                        ProcessCommand.retrieveInstanceId(command)
                );
                CommandResponse response = domainManager.commandReceived(command);

                if (response.getCommandState().equals(CommandResponse.CommandState.TERMINATE)) {
                    listeningState = false;
                    this.handleLogDeletion(domainManager);
                }

                String jsonResponse = response.getAsJSONStringResponse();

                if (response.getDomainManager().getContestState() != DomainManager.ContestState.ACTIVE){
                    listeningState = false;
                    this.handleLogDeletion(domainManager);
                }

                outputStream.println(jsonResponse);
                outputStream.flush();

                if (response.getIncludeLogFile()) {
                    this.handleLogDeletion(domainManager);
                }
            }

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IO Error/ Client, " + socket.toString() + " terminated abruptly");
            e.printStackTrace();
        }
        catch(NullPointerException e){
            LOGGER.log(Level.SEVERE, "Client Socket" + socket.toString() + " Closed");
            e.printStackTrace();
        }

        finally{
            try{
                System.out.println("Connection Closing..");
                if (inputStream != null){
                    inputStream.close();

                    LOGGER.log(Level.INFO, "Socket Input Stream Closed");
                }

                if(outputStream != null){
                    outputStream.close();

                    LOGGER.log(Level.INFO, "Socket Out Closed");
                }
                if (socket != null){
                    socket.close();

                    LOGGER.log(Level.INFO, "Socket Closed");
                }
            }
            catch(IOException ie){
                LOGGER.log(Level.SEVERE,"Socket Close Error");
                ie.printStackTrace();
            }
        }
    }

    /**
     * Retrieves the DomainManager that relates to the Client and Attempt instance.
     *
     * @param clientId
     * @param instanceId
     * @return DomainManager
     */
    private DomainManager getDomainManager(String clientId, String instanceId) {

        DomainManager domainManager;

        String domainId = DomainManager.makeDomainId(clientId, instanceId);

        if (!this.domainManagers.containsKey(domainId)) {
            domainManager = this.generateNewDomainManager(clientId, instanceId);
            this.domainManagers.put(domainId, domainManager);
        }

        domainManager = this.domainManagers.get(domainId);

        return domainManager;
    }

    /**
     * Generates a new Domain Manager for the Clients attempt. Freshly parses the GPT.xml file to generate a new
     * Environment (in case in future each attempt uses different files or configurations).
     *
     * @param clientId represents the client (provided at registration)
     * @param instanceId represents the specific attempt (provided by BDI client)
     * @return DomainManager as newly created.
     */
    private DomainManager generateNewDomainManager(String clientId, String instanceId) {

        return new DomainManager(clientId, instanceId);
    }

    private void handleLogDeletion(DomainManager domainManager) {
        if (this.currentSimulatorMode == Simulator.SimulatorMode.NORMAL) {
            domainManager.getLogger().deleteLogFile();
        }
    }

}
