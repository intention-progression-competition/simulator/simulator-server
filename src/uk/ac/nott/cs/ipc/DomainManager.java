// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * This class is used to group all related methods for DomainManagement of the competition environments and has one
 * per attempt.
 *
 * The DomainManager handles interfacing with the environment for each attempt, including coordinating the processing of
 * all client requests and responses.
 */
public class DomainManager {

    private Environment environment;
    private ProcessCommand processCommand;
    private String clientId;
    private String instanceId;

    private String gptFile = "gpts/gpt.1.xml";

    private int timeoutMinutes = 10;
    private long timeRemaining = 0;
    private boolean competitionTimedout = false;

    private Random randomiser;

    private ArrayList<Long> tickTimes;

    private long randomSeed;
    private final boolean OVERRIDE_CLIENT_RANDOM_SEED = false;
    private final long OVERRIDE_RANDOM_SEED_VALUE = 0;

    private final int TICK_STEPS_LITERAL_FLIP = 5; //how many ticks pass before randomly flipping a literal
    private final int NUM_GOALS = 10; //the target number of goals to prepare
    private final int MIN_GOALS = 2; //the target minimum number of goals to try and maintain

    private int numGoals; //the actual safe number selected
    private int minGoals; //the actual safe minimum


    private int tickCount = 0;
    private int nextGoalTick = 0;

    private SimLog logger;

    public enum ContestState {
        ACTIVE, COMPLETE, TIMEOUT
    }

    /**
     * Instantiates the Domain Manager for this specific attempt
     *
     * @param clientId      Unique identifier representing the client (provided on registration)
     * @param instanceId    Unique identifier representing the attempt instances (provided by the BDI client)
     */
    public DomainManager(String clientId, String instanceId) {
        this.processCommand = new ProcessCommand(this);
        this.clientId = clientId;
        this.instanceId = instanceId;

        this.tickTimes = new ArrayList<>();

        logger = new SimLog(this);
    }

    /**
     * Sets up the Domain Manager, configuring the randomiser (with any seeds), prepares the literals (e.g. sets initial
     * state, and chooses a random goal). Triggers the logging to capture the attempt initiation data.
     */
    public void initiateDomain() {

        logger.writeHeaderInfo();

        GPTParser gpt = new GPTParser();
        gpt.parseGPTFile(this.getGptFile());

        this.environment = gpt.getEnvironment();

        this.setupRandomiser();
        this.prepareLiterals();
        this.setupGoals();
    }

    /**
     * Set up the goals to be picked from
     */
    private void setupGoals() {
        List<String> goalKeySet = new ArrayList(environment.getGoals().keySet());
        Collections.shuffle(goalKeySet, randomiser);

        numGoals = Math.min(goalKeySet.size(), NUM_GOALS);
        environment.getPendingGoals().addAll(goalKeySet.subList(0, numGoals));

        minGoals = Math.min(goalKeySet.size(), MIN_GOALS);

        for (int i = 0; i < minGoals; i++) {
            nextGoalTick = addPendingGoal();
        }
    }

    /**
     * Adds the next goal to the active goals
     * @return a random timestep shorter than the execution path of the goal
     */
    private int addPendingGoal() {
        if (environment.getPendingGoals().isEmpty()){
            throw new RuntimeException("Called to add pending goal from no pending goals.");
        }
        String nextGoal = environment.getPendingGoals().remove(0);
        environment.getActiveGoals().add(nextGoal);
        return randomiser.nextInt(environment.getGoals().get(nextGoal).getExecPathLength());
    }


    /**
     * Configs the random seeding.
     * If overriding of the client specified random seed is permitted, uses the OVERRIDE_RANDOM_SEED_VALUE
     * If not, uses the random seed as specified by the client (either agent or BDI client if not specified)
     * Overriding is to be used for the purpose of running in Live competition state.
     */
    private void setupRandomiser() {

        long seed;

        if (OVERRIDE_CLIENT_RANDOM_SEED) {
            seed = OVERRIDE_RANDOM_SEED_VALUE;
        } else {
            seed = this.getRandomSeed();
        }

        randomiser = new Random(seed);
    }

    /**
     * Prepares the literals ready to be used. Non-goal condition literals are randomly set (true or false)
     * A list of non-goal condition literals are stored for expediency in other methods.
     */
    private void prepareLiterals() {

        Collection<Literal> literals = this.getEnvironment().getLiterals().values();

        for (Literal literal : literals) {
            if (literal.isRandomInit()){
                literal.setState(randomiser.nextBoolean());
            }
            if (literal.isStochastic()) {
                environment.getStochasticLiterals().add(literal.getId());
            }
        }
    }

    /**
     * Requests that the received command is processed by a ProcessCommand object.
     * Triggers the Environment Agent to run (e.g. flipping literals and adding goals).
     * @param command the passed command
     * @return CommandResponse containg all details of the simulators response.
     */
    public CommandResponse commandReceived(String command) {

        boolean returnLogData = false;

        CommandResponse response = this.processCommand.processCommand(command);

        this.processEnvironmentalAgent();

        if (this.isCompetitionTimedout()) {
            this.getLogger().logEvent("Competition Timed Out. (" + (this.getTimeRemaining()) + " milliseconds over the permitted time)");
            this.getLogger().writeFooterInfo();

            returnLogData = true;

        } else if (this.allGoalsComplete()) {

            this.getLogger().logEvent("Competition completed by matching all Goals");
            this.getLogger().writeFooterInfo();

            returnLogData = true;
        }

        if (returnLogData) {
            response.activateLogReturn(this);
        }

        return response;
    }

    /**
     * Process all actions to be undertaken by Environment Agent, randomly flip literals and add goals at
     * appropriate timesteps.
     */
    private void processEnvironmentalAgent() {

        this.incrementTick();

        int currentTick = this.getTickCount();

        if (currentTick % TICK_STEPS_LITERAL_FLIP == 0) {
            this.flipRandomLiteral();
        }

        for (String goal: environment.getActiveGoals()) {
            if(!environment.getCompletedGoals().contains(goal)
                    && goalConditionsMet(environment.getGoals().get(goal))){
                environment.getCompletedGoals().add(goal);
            }
        }
        for (String goal: environment.getCompletedGoals()){
            environment.getActiveGoals().remove(goal);
        }

        while (!environment.getPendingGoals().isEmpty() &&
                ((currentTick == nextGoalTick) || (environment.getActiveGoals().size() < minGoals))) {
            nextGoalTick = currentTick + addPendingGoal();
        }
    }


    /**
     * Flips the state of a literal (chosen at random), for example, will choose literal 4 and then flip from False to True.
     * Will only flip non Goal Condition literals.
     */
    private void flipRandomLiteral() {

        ArrayList<String> stochasticLiteralKeys = this.getEnvironment().getStochasticLiterals();

        if (stochasticLiteralKeys.size() > 0) {
            int randomlySelectedKey = randomiser.nextInt(stochasticLiteralKeys.size());
            Literal randomlyFlippedLiteral = environment.getLiterals().get(stochasticLiteralKeys.get(randomlySelectedKey));
            randomlyFlippedLiteral.flip();

            this.getLogger().logError("Flipped: " + randomlyFlippedLiteral.toString());
        }
    }

    /**
     * Processes an action as specified by the client.
     *
     * @param actionName
     * @throws ActionException if the selected actions preconditions do not hold
     * @return true if action successfully run, false if not.
     */
    public boolean runAction(String actionName) {

        boolean result = false;

        try {
            Action action = this.getActionByName(actionName);

            if (this.validatePreConditionsHolds(action)) {
                ArrayList<Literal> postConditionLiterals = action.getPostConditions();
                for (Literal postConditionLiteral:
                     postConditionLiterals) {
                    this.updateLiteral(postConditionLiteral);
                }
                result = true;
            } else {
                throw new ActionException(actionName, "Precondition for action failed");
            }
        } catch(ActionException exception) {
            this.getLogger().logError("Action " + exception.getActionName() + " failed with: " + exception.getMessage());
        }

        return result;
    }

    /**
     * Gets the action as referenced by it's name
     * @param actionName
     * @return Action as specified by name
     * @throws ActionException if Action not found
     */
    public Action getActionByName(String actionName) throws ActionException {

        Action action = this.getEnvironment().getActions().get(actionName);

        if (action == null) throw new ActionException(actionName, "Action Not Found");

        return action;
    }

    /**
     * Checks whether the preconditions for the specified action hold.
     * @param action
     * @return true if the preconditions for an action hold, false if not.
     */
    private boolean validatePreConditionsHolds(Action action) {
        // Get the list of preconditions
        ArrayList<Literal> preConditions = action.getPreConditions();
        // Get the hashMap of environmental literals
        HashMap<String, Literal> envLits = environment.getLiterals();

        // Loop over the preConditions and ensure they hold
        for (Literal preCond: preConditions) {
            if (!preCond.holds(envLits.get(preCond.getId()))){
                // If any one condition fails return false
                return false;
            }
        }
        return true;
    }

    /**
     * Determines if the attempt has been successfully completed.
     * @return true if ALL current goals have been met, false if not.
     */
    public boolean allGoalsComplete() {
        return (environment.getActiveGoals().isEmpty());
    }

    /**
     * Determines if a goal pre-conditions hold (e.g. literal -2 is true).
     * @param goal
     * @return true if a goals conditions have been met, false if not.
     */
    private boolean goalConditionsMet(Goal goal) {
        ArrayList<Literal> goalConditions = goal.getSuccessConditions();
        HashMap<String, Literal> envLits = environment.getLiterals();

        for (Literal goalCond: goalConditions) {
            if (!goalCond.holds(envLits.get(goalCond.getId()))){
                return false;
            }
        }

        this.getLogger().logEvent("Goal " + goal.getName() + " succeeded on matched list:\n " + goalConditions.toString());
        return true;
    }

    /**
     * Replaces the environmentally held Literal with a new Literal in order to update the state
     * @param targetLiteral representing the literal to be updated.
     */
    private void updateLiteral(Literal targetLiteral) {
        this.getEnvironment().getLiterals().replace(targetLiteral.getId(), targetLiteral.clone());
    }

    public String getClientId() {
        return clientId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String getDomainId() {
        return this.getClientId() + "-" + this.getInstanceId();
    }

    private int incrementTick() {
        return this.tickCount++;
    }

    public int getTickCount() {
        return tickCount;
    }

    public long getRandomSeed() {
        return randomSeed;
    }

    public void setRandomSeed(long randomSeed) {
        this.randomSeed = randomSeed;
    }

    public String getGptFile() {
        return gptFile;
    }

    public void setGptFile(String gptFile) {
        this.gptFile = gptFile;
    }

    /**
     * This extracts the GPT file contents as a single String
     * @return String containg GPT file contents
     */
    public String getGptContents() {

        String gptFileContents = "";
        Charset charset = StandardCharsets.UTF_8;

        try {
            List<String> lines = Files.readAllLines(Paths.get(this.getGptFile()), charset);
            gptFileContents = String.join(System.getProperty("line.separator"), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return gptFileContents;
    }

    public int getTimeoutMinutes() {
        return timeoutMinutes;
    }

    public void setTimeoutMinutes(int timeoutMinutes) {
        this.timeoutMinutes = timeoutMinutes;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public SimLog getLogger() {
        return logger;
    }


    static public String makeDomainId(String clientId, String instanceId) {
        return clientId + "-" + instanceId;
    }

    public ArrayList<Long> getTickTimes() {
        return tickTimes;
    }

    public void addTickTime(Long tickTime) {
        this.getTickTimes().add(tickTime);
        this.updateCompetitionTime();
        this.getLogger().logTickData(tickCount, tickTime, this.getTimeRemaining());
    }

    public long getTotalDuration() {
        return getTickTimes().stream().mapToLong(Long::longValue).sum();
    }

    public ContestState getContestState() {

        ContestState state = ContestState.ACTIVE;

        if (this.allGoalsComplete()) {
            state = ContestState.COMPLETE;
        }

        if (this.getTimeRemaining() <= 0) {
            state = ContestState.TIMEOUT;
        }

        return state;
    }

    private long getTimeoutMilliseconds() {
        return this.getTimeoutMinutes() * 60 * 1000;
    }

    public boolean isCompetitionTimedout() {
        return competitionTimedout;
    }

    public void setCompetitionTimedout(boolean competitionTimedout) {
        this.competitionTimedout = competitionTimedout;
    }

    public void setTimeRemaining(long timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public long getTimeRemaining() {
        return timeRemaining;
    }

    public boolean updateCompetitionTime() {
        this.setTimeRemaining(this.getTimeoutMilliseconds() - this.getTotalDuration());

        if (this.getTimeRemaining() <= 0) {
            this.setCompetitionTimedout(true);
        }

        return this.isCompetitionTimedout();
    }
}
