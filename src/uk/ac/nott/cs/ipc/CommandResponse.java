// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import org.json.JSONObject;

/**
 * This class represents the CommandResponse that will be issued by the simulator
 */
public class CommandResponse {

    private String message;
    private CommandState commandState;
    private DomainManager domainManager;
    private Environment environment;
    private String gptFile;
    private Boolean includeGPT = false;
    private String gptContents;
    private String logFileName;
    private Boolean includeLogFile = false;
    private String logFileContents;

    public enum CommandState {
        VALID_COMMAND, COMMAND_NOT_RECOGNISED, TERMINATE, INVALID_COMMAND, ACTION_FAILED, MISSING_CLIENT_ID, INVALID_GPT_FILE
    }

    public CommandResponse(DomainManager domainManager) {
        this.domainManager = domainManager;
        this.environment = domainManager.getEnvironment();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommandState getCommandState() {
        return commandState;
    }

    public void setCommandState(CommandState commandState) {
        this.commandState = commandState;
    }

    public JSONObject getAsJSONResponse() {
        JSONObject json = new JSONObject();

        if (this.getCommandState() != CommandState.MISSING_CLIENT_ID &&
                this.getCommandState() != CommandState.INVALID_GPT_FILE) {
            json.append("environment", this.getEnvironment().getEnvironmentAsJSON());
        }

        if (this.getIncludeGPT()) {
            json.append("gptfile", this.getGptFile());
            json.append("gptcontents", this.getGptContents());
        }

        if (this.getIncludeLogFile()) {
            json.append("logfile", this.getLogFileName());
            json.append("logcontents", this.getLogContents());
        }

        JSONObject contestStatusJson = new JSONObject();

        contestStatusJson.append("command", this.getCommandState());
        contestStatusJson.append("message", this.getMessage());
        contestStatusJson.append("contest", this.getDomainManager().getContestState());
        contestStatusJson.append("timeremaining", this.getDomainManager().getTimeRemaining());

        json.append("status", contestStatusJson);

        return json;
    }

    public void activateLogReturn(DomainManager domain) {
        this.setIncludeLogFile(true);
        this.setLogFileContents(domain.getLogger().getLogContents());
        this.setLogFileName("logs/" + domain.getLogger().getLogFileName());
    }

    public String getAsJSONStringResponse() {
        return this.getAsJSONResponse().toString();
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public String getGptFile() {
        return gptFile;
    }

    public void setGptFile(String gptFile) {
        this.gptFile = gptFile;
    }

    public Boolean getIncludeGPT() {
        return includeGPT;
    }

    public void setIncludeGPT(Boolean includeGPT) {
        this.includeGPT = includeGPT;
    }

    public String getGptContents() {
        return gptContents;
    }

    public void setGptContents(String gptContents) {
        this.gptContents = gptContents;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    public Boolean getIncludeLogFile() {
        return includeLogFile;
    }

    public void setIncludeLogFile(Boolean includeLogFile) {
        this.includeLogFile = includeLogFile;
    }

    public String getLogContents() {
        return logFileContents;
    }

    public void setLogFileContents(String logFileContents) {
        this.logFileContents = logFileContents;
    }

    public DomainManager getDomainManager() { return domainManager; }
}
