// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

import java.util.ArrayList;

public class Goal {

    private String name;
    private ArrayList<Literal> successConditions;
    private int execPathLength;

    public Goal(String name) {
        this.name = name;
        this.successConditions = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Literal> getSuccessConditions() {
        return successConditions;
    }

    public int getExecPathLength() {
        return execPathLength;
    }

    public void setExecPathLength(int execPathLength) {
        this.execPathLength = execPathLength;
    }
}
