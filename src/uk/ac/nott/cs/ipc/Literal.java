// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
//
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;

public class Literal implements Comparable<Literal> {

    private String id;
    private boolean state;
    private boolean stochastic;
    private boolean randomInit;

    public Literal(String id, boolean state){
        this.id = id;
        this.state = state;
        this.stochastic = true;
        this.randomInit = true;
    }

    public Literal(String id, boolean state, boolean stochastic, boolean randomInit) {
        this.id = id;
        this.state = state;
        this.stochastic = stochastic;
        this.randomInit = randomInit;
    }

    public String getId() {
        return this.id;
    }

    public boolean getState() {
        return state;
    }

    public boolean isStochastic() {
        return stochastic;
    }

    public boolean isRandomInit() {
        return randomInit;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean flip(){
        this.state = !this.state;
        return this.state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Literal literal = (Literal) o;

        return id == literal.id;

    }

    public boolean holds(Literal literal)
    {
        return this.id.equals(literal.getId()) && this.state == literal.getState();
    }

    @Override
    public int compareTo(Literal o) {
        return this.getId().compareTo(o.getId());
    }

    @Override
    public String toString() {
        return (this.getId()) + ":" + (this.getState());
    }

    @Override
    public Literal clone(){
        return new Literal(id, state, stochastic, randomInit);
    }
}
