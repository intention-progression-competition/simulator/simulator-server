// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Handles the logging of each solution attempt
 */
public class SimLog {

    private DomainManager domainManager;

    private int tickCycleId = 0;
    private int timeConsumed;
    private String action;
    private Goal achievedGoal;
    private Environment environment;

    private String logFileName;
    private final String LOG_DIR = "logs";

    public SimLog(DomainManager domainManager) {

        this.domainManager = domainManager;
        this.logFileName = domainManager.getDomainId() + ".txt";
    }

    private void writeToFile(String message) {
        boolean madeDir = false;
        try {
            madeDir = new File(LOG_DIR).mkdir();
            FileWriter fileWriter = new FileWriter(this.getFullLogPath(), true);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(this.getTimeStamp() + ": " + message);
            bufferedWriter.newLine();

            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("mkdir(\"logfiles\") result:" + madeDir);
        }
    }

    /**
     * This extracts the Log file contents as a single String
     * @return String containing Log file contents
     */
    public String getLogContents() {

        String logFileContents = "";
        Charset charset = Charset.forName("UTF-8");

        try {
            List<String> lines = Files.readAllLines(Paths.get(this.getFullLogPath()), charset);
            logFileContents = String.join(System.getProperty("line.separator"), lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return logFileContents;
    }

    public void writeHeaderInfo() {
        this.writeToFile("Client ID: " + this.getDomainManager().getClientId());
        this.writeToFile("Instance ID: " + this.getDomainManager().getInstanceId());
        this.writeToFile("Environment seed: " + this.getDomainManager().getRandomSeed());
        this.writeToFile("Timeout Mins.: " + this.getDomainManager().getTimeoutMinutes());
        this.writeToFile("GPT File: " + this.getDomainManager().getGptFile());
    }

    public void writeFooterInfo() {
        this.writeToFile("Total Ticks: " + this.getDomainManager().getTickCount());
        this.writeToFile("Total Duration: " + this.getDomainManager().getTotalDuration());
    }

    private String getTimeStamp() {

        String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

        return date;
    }

    public void logTickData(int tickCycleId, long tickTime, long timeRemaining) {
        String tickData = "Tick Id: " + tickCycleId +
                ", Duration: " + tickTime +
                ", Remaining: " + timeRemaining;
        this.writeToFile(tickData);
    }

    public void logEvent(String event) {
        this.writeToFile(event);
    }

    public void logError(String error) {
        this.writeToFile(error);
    }

    private void writeToFile()
    {
        /** TODO: left as a reminder....
            Capture the following:
                - Tick cycle Id
                - Time Consumed (received / sent)
                - Action undertaken
                - Error / Failures
                - Achieved Goal
                - Updated State
         */

    }

    public int getTickCycleId() {
        return tickCycleId;
    }

    public void setTickCycleId(int tickCycleId) {
        this.tickCycleId = tickCycleId;
    }

    public int getTimeConsumed() {
        return timeConsumed;
    }

    public void setTimeConsumed(int timeConsumed) {
        this.timeConsumed = timeConsumed;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Goal getAchievedGoal() {
        return achievedGoal;
    }

    public void setAchievedGoal(Goal achievedGoal) {
        this.achievedGoal = achievedGoal;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public DomainManager getDomainManager() {
        return domainManager;
    }
    public void setDomainManager(DomainManager domainManager) {
        this.domainManager = domainManager;
    }

    public String getLogFileName() {
        return logFileName;
    }

    private String getFullLogPath() {
        return LOG_DIR + '/' + this.getLogFileName();
    }

    public void deleteLogFile() {
        File logFile = new File(this.getFullLogPath());
        if(logFile.exists() && !logFile.isDirectory()) {
            if (logFile.delete()) {
                System.out.println("Log file " + domainManager.getLogger().getLogFileName() + " deleted.");
            } else {
                System.out.println("Log file " + domainManager.getLogger().getLogFileName() + " could not be.");
            }
        }
    }
}
